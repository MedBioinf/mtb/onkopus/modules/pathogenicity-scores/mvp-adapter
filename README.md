# MVP-Adapter

Module for retrieving MVP pathogenicity scores [1] of missense variants.

### References

[1] Qi, H., Zhang, H., Zhao, Y., Chen, C., Long, J. J., Chung, W. K., ... & Shen, Y. (2021). MVP predicts the pathogenicity of missense variants by deep learning. Nature communications, 12(1), 510.
