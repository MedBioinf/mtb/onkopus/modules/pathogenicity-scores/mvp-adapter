import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "DATABASE_PATH" in os.environ:
    __DATABASE_PATH__ = os.getenv("DATABASE_PATH")
else:
    __DATABASE_PATH__ = config['DEFAULT']['DATABASE_PATH']

if "DATABASE_FILE" in os.environ:
    __DATABASE_FILE__ = os.getenv("DATABASE_FILE")
else:
    __DATABASE_FILE__ = config['DEFAULT']['DATABASE_FILE']